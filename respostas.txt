/***** MAC0110 - EP3 *****/
  Nome: Luca Assumpção Dillenburg
  NUSP: 11796580

obs: modifiquei o código para os lobos só comerem o coelhos (como está no enunciado do EP3)

/***** Parte 1 - Entendendo o código *****/

    1) Qual a probabilidade de um elemento da matriz ser grama (ou terreno)? Você pode expressar essa possibilidade utilizando um valor numérico ou uma fórmula.
		
		99% * 65%

    2) Analise o código e diga: qual a diferença entre o terreno e o terreno especial?
		
		Os ícones são diferentes e o terreno especial tem menos chance de aparecer. Contudo, fora isso, ambos são iguais.

    3) Dados os valores iniciais das constantes, qual a energia necessária para um lobo se reproduzir? E um coelho?
		
		Olhando as constantes, imagino que a energia necessária para um animal se reproduzir seja FATOR_REPRODUCAO * ENERGIA_ANIMAL. Portanto, o lobo precisaria de 20 e o coelho, 12.

/***** Parte 2 - Completando as funções *****/

    4) Ao contrário das funções ocupa_vizinho! e reproduz!, a função morre! não recebe a matriz de energia como parâmetro. Por quê não é necessário alterar a energia com a morte de um animal?
		
		Porque sua energia já é zero quando ele chama a função e a energia de grama pode ser zero pois ninguém pode comê-la.

/***** Parte 3 - Teste e simulação *****/

    5) Usando os valores iniciais das constantes e após uma quantidade considerável de iterações, mais de 30, por exemplo, o que acontece na ilha?
		
		Os coelhos acabam muito rápido e com a energia adiquirida pelos lobos, muitos deles conseguem se reproduzir. Porém sem mais alimento, eles vão morrendo aos poucos até não sobrar mais nenhum. No final há só cenouras e terrenos.

    6) Qual combinação de constantes leva a ilha a ser dominada por coelhos? Utilize uma ilha de tamanho 20 ou mais, e simule utilizando uma quantidade considerável de iterações.

		PROBABILIDADE_LOBO = 0.01, PROBABILIDADE_COELHO = 0.1, PROBABILIDADE_COMIDA = 0.2

    7) Qual combinação de constantes leva a ilha não ter nenhum animal? Utilize uma ilha de tamanho 20 ou mais, e simule utilizando uma quantidade considerável de iterações.

        PROBABILIDADE_LOBO = 0.05, PROBABILIDADE_COELHO = 0.5, PROBABILIDADE_COMIDA = 0.2

/***** Parte 3 - Usando DataFrames e plotando gráficos *****/

    8) Qual a diferença entre a função simula - da parte 3 - e a função simula2?

        A função simula2 printa os gráficos dependendo do parâmetro e retorna os dados necessários para plotar os gráficos das iterações.

    9) A função gera_graficos possui sintaxes diferentes das vistas em aula e nos outros exercícios. Apesar disso, é possível entender o que a função faz, sem rodá-la e sem conhecer detalhes sobre os pacotes de gráficos. Sem rodar a função, responda: quantos gráficos a função plota? Qual o conteúdo de cada gráfico?

        A função mostra um gráfico de quantidade de lobos e coelhos durante o tempo e outro de energia e comida durante o tempo.

    10) Usando os valores iniciais das constantes e após uma quantidade considerável de iterações, mais de 30, por exemplo, o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu na parte 3?

        O número de coelhos começa bem maior que o de lobos, então os lobos comem vários coelhos, fazendo com que o número de lobos fique maior do que o número de coelhos. No final ambos ficam próximos de zero.

    11) Usando a combinação de constantes da questão 6 - que leva a ilha a ser dominada por coelhos - o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu naquela questão?

        O gráfico se assemelha muito a análise feita na questão, porém ficou mais simples de analisar de forma geral o que aconteceu ao passar do tempo. O número de coelhos começa maior do que o número de lobos, então o número de coelhos cresce por conta de reproduções e depois desce por muitos serem predados pelos lobos. Próximo da 25o iteração, o número de lobos passa o número de coelhos e, no final, o número de coelhos passa o número de lobos novamente, que fizaliza em zero.

    12) Usando a combinação de constantes da questão 7 - que leva a ilha à extinção de todos os animais - o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu naquela questão?

        O gráfico se assemelha muito a análise feita na questão, porém ficou mais simples de analisar de forma geral o que aconteceu ao passar do tempo. O número de coelhos começa muito maior do que o número de lobos, os quais rapidamente matam todos os coelhos. Então conseguem se produzir, aumentando sua população, mas ficam sem alimento e vão diminuindo até chegar em zero.
